<%-- 
    Document   : newjsp
    Created on : 2022-05-27, 13:51:53
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dodane liczby:</h1>
        <%
            String numer1=request.getParameter("n1");
            String numer2=request.getParameter("n2");
            String numer3=request.getParameter("n3");
            String numer4=request.getParameter("n4");
        %>
        <jsp:useBean id="dBean" class="wDomu.DataBean" scope="session"></jsp:useBean>
        <jsp:setProperty name="dBean" property="licz1" value="<%=numer1%>" />
        <jsp:setProperty name="dBean" property="licz2" value="<%=numer2%>" />
        <jsp:setProperty name="dBean" property="licz3" value="<%=numer3%>" />
        <jsp:setProperty name="dBean" property="licz4" value="<%=numer4%>" /><br>
        <i><jsp:getProperty name="dBean" property="licz1" /></i><br>
        <i><jsp:getProperty name="dBean" property="licz2" /></i><br>
        <i><jsp:getProperty name="dBean" property="licz3" /></i><br>
        <i><jsp:getProperty name="dBean" property="licz4" /></i><br><br />
        Podaje wartosc max z tych liczb:
        <i><jsp:getProperty name="dBean" property="maxim" /></i><br /><br />
        <meta http-equiv="refresh" content="5; URL=index.html">
    </body>
    <form action="wylogowanie.jsp" method="POST">
            <input type="submit" value="Wyloguj" />
    </form><br /><br />
    <a class="button rounded-full-btn reload-btn s-2 margin-bottom" href="index.html"><i class='icon-sli-arrow-left'></i>Powrót</a>
</html>
