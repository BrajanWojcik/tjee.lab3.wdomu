<%-- 
    Document   : wynik
    Created on : 2022-05-31, 19:36:35
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Losowanie</title>
    </head>
    <body>
        <h1>Losowanie liczb</h1>
        <%
            String liczba=request.getParameter("gr");
        %>
        <jsp:useBean id="dBean" class="wDomu.DataBean" scope="session"></jsp:useBean>
        <jsp:setProperty name="dBean" property="gran" value="<%=liczba%>" />
        Liczby zostaly losowane z zakresu: 0 - 
        <i><jsp:getProperty name="dBean" property="gran1" /></i><br>
        Wylosowane liczby:
        <br />
        <i><jsp:getProperty name="dBean" property="licz1" /></i><br>
        <i><jsp:getProperty name="dBean" property="licz2" /></i><br>
        <i><jsp:getProperty name="dBean" property="licz3" /></i><br>
        <i><jsp:getProperty name="dBean" property="licz4" /></i><br><br />
        Podaje wartosc max z tych liczb:
        <i><jsp:getProperty name="dBean" property="maxim" /></i><br /><br />
        <meta http-equiv="refresh" content="5; URL=index.html">
    </body>
    <form action="wylogowanie.jsp" method="POST">
            <input type="submit" value="Wyloguj" />
    </form><br />
    <a class="button rounded-full-btn reload-btn s-2 margin-bottom" href="index.html"><i class='icon-sli-arrow-left'></i>Powrót</a>
</html>
