/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wDomu;

import java.beans.*;
import java.io.*;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author student
 */
public class DataBean implements Serializable {
    
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    private String sampleProperty;
    private String licz1;
    private String licz2;
    private String licz3;
    private String licz4;
    private String gran1;
    private int maxim;
    private int gran;
    Random rand = new Random();
    private String legend;
    private String dana;
    
    private PropertyChangeSupport propertySupport;
    
    public DataBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public String getSampleProperty() {
        return sampleProperty;
    }
    
    public String getlicz1() {
        return licz1;
    }
    
    public String getlicz2() {
        return licz2;
    }
    
    public String getlicz3() {
        return licz3;
    }
    
    public String getlicz4() {
        return licz4;
    }
    
    public int getmaxim() throws IOException {
        setmaxim();
        zapishistori(maxim);
        return maxim;
    }
    
    public String getgran1() {
        gran1=String.valueOf(gran);
        return gran1;
    }
    
    public void setSampleProperty(String value) {
        String oldValue = sampleProperty;
        sampleProperty = value;
        propertySupport.firePropertyChange(PROP_SAMPLE_PROPERTY, oldValue, sampleProperty);
    }
    
    public void setlicz1(String value) {
        licz1 = value;
    }
    
    public void setlicz2(String value) {
        licz2 = value;
    }
    
    public void setlicz3(String value) {
        licz3 = value;
    }
    
    public void setlicz4(String value) {
        licz4 = value;
    }
    
    public void setgran(String value) {
        gran = Integer.parseInt(value);
        losowanie();
    }
    
    public void setmaxim() {
        int l1=Integer.parseInt(licz1);
        int l2=Integer.parseInt(licz2);
        int l3=Integer.parseInt(licz3);
        int l4=Integer.parseInt(licz4);
        maxim = Math.max(Math.max(l1, l2),Math.max(l3, l4));
    }
    
    public void losowanie() {
        int l1=rand.nextInt(gran+1);
        licz1=String.valueOf(l1);
        int l2=rand.nextInt(gran+1);
        licz2=String.valueOf(l2);
        int l3=rand.nextInt(gran+1);
        licz3=String.valueOf(l3);
        int l4=rand.nextInt(gran+1);
        licz4=String.valueOf(l4);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    public void zapishistori(int value) throws IOException{
        BufferedWriter zapis = new BufferedWriter(new FileWriter("/home/student/NetBeansProjects/Zadanie31.H/historia.txt", true));
        legend=String.valueOf(value);
        zapis.append("Max: ");
        zapis.append(legend);
        zapis.append("\n");
        zapis.close();
    }
    
    public void wypishistori() throws FileNotFoundException{
        File plik = new File("/home/student/NetBeansProjects/Zadanie31.H/historia.txt");
        Scanner czytam = new Scanner(plik);
        while(czytam.hasNextLine()){
            dana=czytam.nextLine();
            System.out.println(dana);
        }
        czytam.close();
    }
    
}
