<%-- 
    Document   : wylogowanie
    Created on : 2022-05-31, 22:24:46
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wylogowanie</title>
    </head>
    <body>
        <div>Pozytywnie wylogowano uzytkownika</div><br />
        <form action="login.jsp" method="POST">
            <input type="submit" value="Zaloguj ponownie" />
        </form>
        <meta http-equiv="refresh" content="5; URL=login.jsp">
    </body>
</html>
