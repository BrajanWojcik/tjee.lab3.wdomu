<%-- 
    Document   : dodanie
    Created on : 2022-05-27, 23:49:13
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wprowadzenie</title>
    </head>
    <body>
        <h1>Wprowadz liczby</h1>
        <form action="newjsp.jsp" method="POST">
            <p>Wprowadz liczbe 1:</p>
            <input type="text" name="n1" required/>
            <p>Wprowadz liczbe 2:</p>
            <input type="text" name="n2" required/>
            <p>Wprowadz liczbe 3:</p>
            <input type="text" name="n3" required/>
            <p>Wprowadz liczbe 4:</p>
            <input type="text" name="n4" required/>
            <p></p>
            <input type="submit" value="Wyznacz MAX" name="confirmButton"/><br /><br />
            <a class="button rounded-full-btn reload-btn s-2 margin-bottom" href="index.html"><i class='icon-sli-arrow-left'></i>Powrót</a>
        </form>
        <meta http-equiv="refresh" content="20; URL=index.html">
    </body>
</html>
