<%-- 
    Document   : ustPar
    Created on : 2022-05-31, 19:31:39
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Granica</title>
    </head>
    <body>
        <h1>Wprowadz granice do losowania</h1>
        <form action="losuj.jsp" method="POST">
            <p>Wprowadz granice:</p>
            <input type="text" name="gr" required/>
            <p></p>
            <input type="submit" value="Wyznacz MAX" name="confirmButton"/><br /><br />
            <a class="button rounded-full-btn reload-btn s-2 margin-bottom" href="index.html"><i class='icon-sli-arrow-left'></i>Powrót</a>
        </form>
        <meta http-equiv="refresh" content="10; URL=index.html">
    </body>
</html>
