<%-- 
    Document   : bledneLogowanie
    Created on : 2022-05-28, 00:11:06
    Author     : student
--%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bledne logowanie</title>
    </head>
    <body>
        <h1><h:outputText value="Błąd logowania?"/></h1>
        <h:form id="formularz_error">
        <h:outputText value="Podana nazwa użytkownika: #{logowanie.nazwa} jest nieprawidłowa, lub podano błędne dla niej hasło! "/><br /><br />
        <h:commandButton value="Spróbuj ponownie" action="retry" />
    </h:form>
    <meta http-equiv="refresh" content="5; URL=login.jsp">
    </body>
</html>
</f:view>
